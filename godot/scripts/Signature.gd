extends ColorRect


var sig_length = 0
var signature = [
	Vector2i(84, 51), Vector2i(85, 52), Vector2i(87, 52), Vector2i(89, 52), 
	Vector2i(91, 52), Vector2i(92, 51), Vector2i(92, 50), Vector2i(91, 49), 
	Vector2i(89, 49), Vector2i(88, 50), Vector2i(88, 51), Vector2i(87, 53), 
	Vector2i(86, 54), Vector2i(86, 55), Vector2i(85, 56), Vector2i(85, 57), 
	Vector2i(84, 58), Vector2i(84, 59), Vector2i(83, 60), Vector2i(83, 61), 
	Vector2i(82, 62), Vector2i(82, 63), Vector2i(81, 64), Vector2i(79, 64), 
	Vector2i(78, 64), Vector2i(77, 63), Vector2i(77, 62), Vector2i(78, 61), 
	Vector2i(80, 61), Vector2i(81, 62), Vector2i(84, 62), Vector2i(85, 63), 
	Vector2i(86, 63), Vector2i(87, 64), Vector2i(89, 64), Vector2i(91, 64), 
	Vector2i(93, 64), Vector2i(95, 64), Vector2i(97, 64), Vector2i(99, 64), 
	Vector2i(101, 64), Vector2i(103, 64), Vector2i(105, 64), Vector2i(107, 64), 
	Vector2i(109, 64), Vector2i(111, 64), Vector2i(113, 64), Vector2i(115, 64), 
	Vector2i(117, 64), Vector2i(119, 64), Vector2i(121, 64), Vector2i(123, 64), 
	Vector2i(125, 64), Vector2i(127, 64), Vector2i(129, 64), Vector2i(131, 64), 
	Vector2i(133, 64), Vector2i(135, 64), Vector2i(137, 64), Vector2i(139, 64), 
	Vector2i(141, 64), Vector2i(143, 64), Vector2i(145, 64), Vector2i(147, 64), 
	Vector2i(149, 64), Vector2i(151, 64), Vector2i(153, 64), Vector2i(155, 64), 
	Vector2i(157, 64), Vector2i(159, 64), Vector2i(161, 64), Vector2i(163, 64), 
	Vector2i(165, 64), Vector2i(167, 64), Vector2i(169, 64), Vector2i(171, 64), 
	Vector2i(173, 64), Vector2i(175, 64), Vector2i(177, 64), Vector2i(179, 64), 
	Vector2i(181, 64), Vector2i(183, 64), Vector2i(185, 64), Vector2i(187, 64), 
	Vector2i(189, 64), Vector2i(191, 64), Vector2i(193, 64), Vector2i(195, 64), 
	Vector2i(197, 64), Vector2i(199, 64), Vector2i(201, 64), Vector2i(202, 64), 
	Vector2i(203, 63), Vector2i(204, 63), Vector2i(205, 62), Vector2i(94, 56), 
	Vector2i(94, 57), Vector2i(93, 58), Vector2i(93, 59), Vector2i(92, 60), 
	Vector2i(92, 61), Vector2i(93, 62), Vector2i(95, 62), Vector2i(96, 62), 
	Vector2i(97, 61), Vector2i(97, 60), Vector2i(98, 59), Vector2i(98, 58), 
	Vector2i(99, 57), Vector2i(99, 56), Vector2i(98, 55), Vector2i(96, 55), 
	Vector2i(95, 55), Vector2i(105, 55), Vector2i(106, 56), Vector2i(106, 57), 
	Vector2i(105, 58), Vector2i(105, 59), Vector2i(104, 60), Vector2i(104, 61), 
	Vector2i(103, 62), Vector2i(107, 56), Vector2i(108, 55), Vector2i(110, 55), 
	Vector2i(119, 55), Vector2i(117, 55), Vector2i(116, 55), Vector2i(115, 56), 
	Vector2i(115, 57), Vector2i(114, 58), Vector2i(114, 59), Vector2i(113, 60), 
	Vector2i(113, 61), Vector2i(114, 62), Vector2i(116, 62), Vector2i(117, 61), 
	Vector2i(118, 60), Vector2i(119, 59), Vector2i(119, 58), Vector2i(120, 57), 
	Vector2i(120, 56), Vector2i(121, 55), Vector2i(121, 54), Vector2i(122, 53), 
	Vector2i(122, 52), Vector2i(123, 51), Vector2i(123, 50), Vector2i(118, 61), 
	Vector2i(119, 62), Vector2i(139, 50), Vector2i(139, 51), Vector2i(138, 52),
	Vector2i(138, 53), Vector2i(137, 54), Vector2i(137, 55), Vector2i(136, 56),
	Vector2i(136, 57), Vector2i(135, 58), Vector2i(135, 59), Vector2i(134, 60),
	Vector2i(134, 61), Vector2i(133, 62), Vector2i(140, 49), Vector2i(142, 49),
	Vector2i(143, 49), Vector2i(144, 50), Vector2i(144, 51), Vector2i(143, 52),
	Vector2i(143, 53), Vector2i(142, 54), Vector2i(142, 55), Vector2i(140, 55),
	Vector2i(143, 56), Vector2i(143, 57), Vector2i(142, 58), Vector2i(142, 59),
	Vector2i(141, 60), Vector2i(141, 61), Vector2i(140, 62), Vector2i(138, 62),
	Vector2i(136, 62), Vector2i(135, 61), Vector2i(150, 55), Vector2i(151, 56),
	Vector2i(151, 57), Vector2i(150, 58), Vector2i(150, 59), Vector2i(149, 60),
	Vector2i(149, 61), Vector2i(148, 62), Vector2i(152, 56), Vector2i(153, 55),
	Vector2i(155, 55), Vector2i(161, 55), Vector2i(160, 56), Vector2i(160, 57),
	Vector2i(159, 58), Vector2i(159, 59), Vector2i(158, 60), Vector2i(158, 61),
	Vector2i(157, 62), Vector2i(162, 52), Vector2i(162, 53), Vector2i(169, 52),
	Vector2i(169, 53), Vector2i(168, 54), Vector2i(168, 55), Vector2i(167, 56),
	Vector2i(167, 57), Vector2i(166, 58), Vector2i(166, 59), Vector2i(165, 60),
	Vector2i(165, 61), Vector2i(166, 62), Vector2i(168, 62), Vector2i(169, 62),
	Vector2i(170, 61), Vector2i(165, 55), Vector2i(166, 55), Vector2i(170, 55),
	Vector2i(178, 55), Vector2i(177, 56), Vector2i(177, 57), Vector2i(176, 58),
	Vector2i(176, 59), Vector2i(175, 60), Vector2i(175, 61), Vector2i(174, 62),
	Vector2i(179, 52), Vector2i(179, 53), Vector2i(187, 56), Vector2i(186, 55),
	Vector2i(184, 55), Vector2i(183, 55), Vector2i(182, 56), Vector2i(182, 57),
	Vector2i(183, 58), Vector2i(184, 59), Vector2i(185, 60), Vector2i(185, 61),
	Vector2i(184, 62), Vector2i(182, 62), Vector2i(181, 62), Vector2i(180, 61),
	Vector2i(197, 50), Vector2i(197, 51), Vector2i(196, 52), Vector2i(196, 53),
	Vector2i(195, 54), Vector2i(195, 55), Vector2i(194, 56), Vector2i(194, 57),
	Vector2i(193, 58), Vector2i(193, 59), Vector2i(192, 60), Vector2i(192, 61),
	Vector2i(191, 62), Vector2i(197, 55), Vector2i(198, 55), Vector2i(199, 56),
	Vector2i(199, 57), Vector2i(198, 58), Vector2i(198, 59), Vector2i(197, 60),
	Vector2i(197, 61), Vector2i(198, 62)]
var sig_size = Vector2i(1, 1)
var sig_color = Color.WHITE

var ticks_per_dot = 1.0/30.0  # 30 frames/sec
var ticks = 0.0
var draw_signature = true

func _draw():
	for i in range(sig_length):
		var spot = Rect2i(signature[i], sig_size)
		draw_rect(spot, sig_color)


func _ready():
	pass # Replace with function body.


func _process(delta):
	if draw_signature:
		ticks += delta
		if ticks > ticks_per_dot:
			sig_length += 1
			if sig_length > len(signature):
				sig_length = len(signature)
				draw_signature = false
			else:
				queue_redraw()
			ticks -= ticks_per_dot
