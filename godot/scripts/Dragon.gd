extends AnimatedSprite2D


var ticks_per_line = 1.0/15.0  # 15 frames/sec
var ticks = 0.0
var drop_down = true


func _ready():
	var intro = [
		1, 0, 1, 1, 0, 1, 2, 3, 4, 2, 4, 2, 6, 8, 6, 8, 6, 8, 6, 8, 6, 8, 6, 8, 
		10, 10, 10, 11, 11, 12, 13, 13, 13, 11, 11, 0, 2, 4, 2, 6, 8, 6, 8, 10, 
		11, 0, 14, 14, 16, 16, 17, 16, 16, 17, 16, 16, 14, 0, 11, 11, 0, 2, 4, 
		2, 0, 2, 4, 6, 6, 6, 7, 9, 9, 7, 9, 11, 11, 13, 12, 1, 1, 1, 1, 1, 10, 
		8, 17, 17, 17, 8, 4, 2
	]
	var dragon_frames = []

	# Extract frame data from "default" animation.
	var num_frames = self.sprite_frames.get_frame_count('default')
	for i in range(num_frames):
		dragon_frames.append(self.sprite_frames.get_frame_texture('default', i))
		
	# Create "intro" animation based on data from Ultima IV.
	self.sprite_frames.add_animation('intro')
	for i in range(len(intro)):
		self.sprite_frames.add_frame('intro', dragon_frames[intro[i]])

	self.animation = 'intro'
	
	# Hide above the screen.
	self.offset.y = -dragon_frames[0].get_height()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if drop_down:
		ticks += delta
		if ticks > ticks_per_line:
			self.offset.y += 1
			if self.offset.y > 0:
				self.offset.y = 0
				drop_down = false
			ticks -= ticks_per_line
